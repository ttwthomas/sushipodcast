---
title: "Ep. 08 - Retour sur le keynote Apple: It's Showtime"
date: 2019-04-03
draft: false
tags: ["apple","services","keynote"]
---
<iframe src="https://anchor.fm/sushipodcast/embed/episodes/Retour-sur-le-keynote-Apple-Its-Showtime-e3kpqb" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>

Cette semaine, le Sushi Podcast discute de la dernière conférence Apple autour des services. Quel services ont été annoncés ? Que représente cela pour Apple et ses utilisateurs ? et finalement qu'en pense le Sushi Gang ?

Liens du podcast :

* [Video de la Keynote](https://www.youtube.com/watch?v=TZmBoMZFC8g)
* [Apple News+](https://www.apple.com/ca/fr/apple-news/)
* [Apple Card](https://www.apple.com/apple-card/)
* [Apple Arcade](https://www.apple.com/ca/fr/apple-arcade/)
* [Apple TV+](https://www.apple.com/ca/fr/apple-tv-plus/)

Suivez-nous sur [Twitter](https://twitter.com/lesushipodcast)  
Abonnez-vous sur [iTunes](https://itunes.apple.com/podcast/sushi-podcast/id1062400528)
