---
title: "Ep. 07 - L'avènement du cloud gaming"
date: 2019-03-25
draft: false
tags: ["jeux","cloud","stadia"]
---
<iframe src="https://anchor.fm/sushipodcast/embed/episodes/Lavnement-du-cloud-gaming-e3hre7" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>

Cette semaine, le Sushi Podcast reviens sur la grosse annonce de la GDC : Google qui se lance dans les jeux vidéo! Quelle est leur stratégie ? Pourquoi ? Comment ? Que fait la concurrence, mais surtout est-ce que l'avenir des jeux vidéo est hors d'une simple boîte ?

Liens du podcast :

* [Game Developer Conference](https://gdconf.com/)
* [Google Stadia](https://store.google.com/magazine/stadia)
* [Google Stadia Dev](https://stadia.dev/)
* [Onlive](https://fr.wikipedia.org/wiki/OnLive)
* [Gaikai](https://fr.wikipedia.org/wiki/Gaikai)
* [Microsoft Game Stack](https://developer.microsoft.com/fr-fr/games)
* [Projet xCloud](https://blogs.microsoft.com/blog/2018/10/08/project-xcloud-gaming-with-you-at-the-center/)
* [Cadence of Hyrule](https://www.youtube.com/watch?v=oYd_pph6RnI)
* [AirPods avec boîtier de charge sans fil](https://www.apple.com/ca/airpods/)
* [Onewheel Pint](https://onewheel.com/products/pint)

Suivez-nous sur [Twitter](https://twitter.com/lesushipodcast)  
Abonnez-vous sur [iTunes](https://itunes.apple.com/podcast/sushi-podcast/id1062400528)